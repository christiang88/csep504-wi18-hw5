# Deliverables

[Pre-proposal](https://docs.google.com/document/d/1IqzS_Eyl3fNnX38zA6QucLKTyvTu2Wj8QuMU8SIKA4w/)

[Proposal](https://docs.google.com/document/d/17QD2tQaZHWoNMAb7w63u_0p5S8Eq4mMAOhVixr93nlA/)

[Design](https://docs.google.com/document/d/1iqrbBX2LjRTyhuVDhbvuz2_TrcskRluwgDVABmD__lY/)

[Report](https://docs.google.com/document/d/107RgXeV3bMhX6mCvUtUE9sCFgZOCPNWVuLFPw3L4hKc/)

[Poster Slides](https://docs.google.com/presentation/d/1v72u-uk5nj8dbIyZOnyB1uymI_kjdNRHSOmMgZ9FeLI/)

# Getting Started

To clone the repository, download all dependencies, and build and run the ShellScript shell, first [install Node.js](https://nodejs.org/dist/v9.8.0/node-v9.8.0-x64.msi) and the [Java 8 SDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and then run the following commands:

```text
$ git clone https://bitbucket.org/christiang88/csep504-wi18-hw5.git shellscript
$ cd shellscript/shell
$ npm install
$ npm start
```

Although the ShellScript shell is intended to be cross-platform, we have developed and tested it exclusively on Windows 10.

# ShellScript Language

Our ShellScript language is a (non-strict) superset of JavaScript, meaning that most programs that are legal JavaScript programs are also legal ShellScript programs that are semantically equivalent (and those that are not would require only minimal modification).

Our language extends JavaScript in four ways: the pipeline operator, naked function calls, naked options arguments, and naked lambda arguments.

## Pipeline Operator

The pipeline operator allows the user to chain multiple function calls, where the functions do not otherwise support a fluent interface.

```text
λ foo(a, b, c) -> bar(x, y, z)
```

The semantics of the pipeline operator are that the left-hand side expression is inserted as the first argument of the function call that is the right-hand side expression. Therefore, the construction above is semantically equivalent to the following:

```text
λ bar(foo(a, b, c), x, y, z)
```

## Naked Function Calls

Naked function calls allow the user to call a function without having to type as many special characters. The naked function call syntax is similar to the Ruby function call syntax, where the function identifier is followed by one or more whitespace-separated arguments.

```text
λ foo a b c
```

The construction above is semantically equivalent to the following:

```text
λ foo(a, b, c)
```

Because passing functions by identifier is a common JavaScript idiom, it is necessary to differentiate between `foo` the function and `foo` the zero-argument call of `foo`. We do that by requiring the user to type parentheses -- `foo()` -- to make a zero-argument call.

And because template literals can be parameters (including the first parameter), this construct means that our language does not support tagged template literals that are supported in modern JavaScript. Since the need for such tagged template literals in a shell language is not great, we feel that this is an appropriate trade-off.

## Naked Options Arguments

Specifying (often optional) arguments by passing an object of named arguments as the last parameter to a function is a common JavaScript idiom. Like naked function calls, naked options arguments allow the user to call such functions without having to type as many special characters.

```text
λ ls 'path' recurse:true
```

Naked options arguments may only appear in naked function calls (above) and must appear as the last arguments to that function calls (except for naked lambda arguments below). They consist of one or more whitespace-separated key-value pairs, with keys separated from values by a colon similar to the standard JavaScript object notation. Therefore, the construction above is semantically equivalent to the following:

```text
λ ls('path', { recurse: true })
```

## Naked Lambda Arguments

Although the arrow function syntax in modern JavaScript is already very concise, our language introduces an even more concise construction for passing a lambda as an argument, provided that it is the last argument.

The syntax comes in two varieties, both of which take inspiration from the Swift language. The first and more useful construction does not require the user to specify the arguments.

```text
λ data -> sort { $0.name < $1.name }
```

This is semantically equivalent to the following:

```text
λ sort(data, ($0, $1) => $0.name < $1.name)
```

The second allows the user to name the arguments, and the equivalent JavaScript construction follows naturally from the above.

```text
λ data -> sort { (a, b) in a.name < b.name }
λ data -> filter { a in a.name.length < 42 }
```

Both constructions above support multiple statements in the body, in which case an explicit `return` is required if the function needs to return something other than `undefined`.

# Built-in Scriptlets

## Foundational Scriptlets

### each

Executes a provided function once for each item.

```text
λ [1, 2, 3] -> each { /* do work with each item in $0 */ }
```

### filter

Passes through only values that pass the test implemented by the provided function.

```text
λ [1, 2, 3] -> filter { $0 > 2 }   // prints 3
```

### map

Creates new values being the results of calling the provided function on every element in the passed in parameter.

```text
λ [1, 2, 3] -> map { $0 * $0 }   // prints 1, 4, 9
```

### reduce

Applies a function against an accumulator and each element in the data to reduce it to a single value. An initial value can be provided optionally.

```text
λ [2, 3, 4] -> reduce { $0 + $1 }      // prints 9
λ [2, 3, 4] -> reduce 1 { $0 * $1 }    // prints 24
```

### sum

Returns the sum of the data elements.

```text
λ [2, 3, 4] -> sum()    // prints 9
```

### product

Returns the product of the data elements.

```text
λ [2, 3, 4] -> product()    // prints 24
```

### average

Returns the average of the data elements.

```text
λ [2, 3, 4] -> average()    // prints 3
```

### sort

Sorts the objects (with an optional comparator function, if specified).

```text
λ [2, 1, 3] -> sort()                   // prints 1, 2, 3
λ ls() -> sort { $0.size - $1.size }    // prints files sorted by size
```

### first

Gets the first n items.

```text
λ [1, 2, 3] -> first()   // prints 1
λ [1, 2, 3] -> first 2   // prints 1 and 2
```

### last

Gets the last n items.

```text
λ [1, 2, 3] -> last()    // prints 3
λ [1, 2, 3] -> last 2    // prints 2 and 3
```

### print

Prints values to the console.

```text
λ print 42
```

### man

Prints help about the passed in function, or all commands if no function is passed.

```text
λ man()
λ man ls
```

### clear

Clears the screen.

```text
λ clear()
```

## Common Commands

### cd

Changes the current working directory.

```text
λ cd '..'
λ cd 'directory/path'
```

### ls

Gets a list of files and directories from the current path, or the specified path.

```text
λ ls()
λ ls recurse:true
```

### mv

Moves, or renames, the specfied file or directory on your filesystem.

```text
λ mv 'from.txt' 'to.txt'
```

### touch

Creates a new, empty file with the specified name or updates the timestamp of the file or directory if it already exists.

```text
λ touch 'file.txt'
```

### mkdir

Creates a new directory with the specified name if it does not already exist.

```text
λ mkdir 'directory'
```

### rm

Deletes the specified file.

```text
λ rm 'file.txt'
```

### rmdir

Removes the specified directory from your filesystem.

```text
λ rmdir 'directory/path'
```

### read

Reads the specified file from your filesystem. Specify format:'json' to read as JSON.

```text
λ read 'file.txt'
λ read 'file.json' format:'json'
```

### write

Writes the data to the specified file. Specify format:'json' to write as JSON.

```text
λ [1, 2, 3] -> write 'file.txt'
λ [1, 2, 3] -> write 'file.json' format:'json'
```

### exec

Runs the executable with the specified name and arguments.

```text
λ exec 'icacls' 'file.txt'
```

The `exec` scriptlet is aliased to `$`.

```text
λ $ 'icacls' 'file.txt'
```

## Fun Commands

### youtube

Plays the YouTube video at the specified URL.

```text
λ youtube 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
```

# Scriptlet API

This section describes the API that ShellScript exposes for writing custom scriptlets.

## ShellScript.iterate(data)

Accepts data of any type and returns an iterator over that data.

If the data is a scalar value (such as string, number, object, or function), an iterator over one element is returned.

If the data is an iterable value (such as an array, Map, Set, or other iterable), an iterate over each element in that data is returned.

```js
// Prints 'foo'
for (let item of ShellScript.iterate('foo')) {
  print(item);
}

// Prints 'foo' and 'bar'
for (let item of ShellScript.iterate(['foo', 'bar'])) {
  print(item);
}

// Prints all values generated by generatorFunc
for (let item of ShellScript.iterate(generatorFunc())) {
  print(item);
}
```

## ShellScript.format(data, formatter)

Tags the data with the specified formatter function.

The data can be the actual data returns (a scalar value like a string, number, or object or an array) or an iterator that returns the data values (such as that returned by a generator function).

The formatter function is a function that accepts the data, and returns a JSX.Element (essentially, HTML and JavaScript) representation of that data to be displayed to the user. The function is only invoked if the data is displayed to the user.

This function returns the data, so you can return directly with it.

```text
export function myScriptlet() {
  const data = 'My Data!';
  return ShellScript.format(data, data => <h1>{data}</h1>);
}
```

If the data is passed or piped to another function or scriptlet, the function will not be invoked, so the function will have access to the raw data, rather than the formatted output. See ShellScript.formatSameAs below if you want to write a scriptlet that modifies data but preserves its any custom formatting.

## ShellScript.formatTable(data, columns)

Tags the data to be formatted with the specified columns. The columns must correspond to the names of properties on the data objects.

```js
export function myScriptlet() {
  const data = [
    { name: 'Bob Smith', age: 29, occupation: 'Student' },
    { name: 'Jane Smith', age: 31, occupation: 'Doctor' },
    { name: 'Alex Johnson', age: 81, occupation: 'Retired' }
  ];
  return ShellScript.formatTable(data, ['name', 'age']);
}
```

Note that, by default, data is formatted in a table with every property of the data objects displayed, so you do not have to use this function if you just want to format as a table with all of the properties.

## ShellScript.formatSameAs(data, sourceData)

Tags the data with the formatter function that was registered on the sourceData, if any. This is useful if you want to return a subset of some source data, and you want to preserve any custom formatting logic that might be attached to that data.

```js
export function myScriptlet(sourceData) {
  const data = sourceData.filter(/* ... */);
  return ShellScript.formatSameAs(data, sourceData);
}
```

## ShellScript.man(func, description)

Registers documentation for the specified function that a user can query with the `man` scriptlet.

```js
export function myScriptlet() {
  // Do something cool
}

ShellScript.man(myScriptlet, 'My scriptlet does something cool.');
```

## Example Scriptlet

The following shows an implementation of the `filter` scriptlet as a generator function that copies the formatter from the source data.

```js
import { ShellScript } from '../../api/ShellScript';

function* filterInner(data, predicate) {
  for (let item of ShellScript.iterate(data)) {
    if (predicate(item)) {
      yield item;
    }
  }
}

export function filter(data, predicate) {
  const result = filterInner(data, predicate);
  return ShellScript.formatSameAs(result, data);
}

ShellScript.man(filter, 'Passes through only values that pass the test implemented by the provided function.');
```
