import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { Shell } from './components/Shell';

const root = document.getElementById('root')!;

ReactDOM.render(<Shell />, root);
