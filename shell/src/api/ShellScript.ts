import { registerMan } from '../components/ManManager';
import { copyPrinter, registerPrinter, registerTablePrinter } from '../components/PrintManager';
import { toIterator } from '../components/toIterator';

// Provides functinos for implementing custom scriptlets.
export const ShellScript = {
  format: registerPrinter,
  formatSameAs: copyPrinter,
  formatTable: registerTablePrinter,
  iterate: toIterator,
  man: registerMan
};
