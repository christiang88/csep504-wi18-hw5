import { ShellScript } from '../../api/ShellScript';
import { getMan, getAllMan } from '../../components/ManManager';

export function man(func?: Function) {
  if (func) {
    return getMan(func) || 'Please consult Google.';
  } else {
    return getAllMan();
  }
}

ShellScript.man(man, 'Prints help about the passed in function, or all commands if no function is passed.');
