import { ShellScript } from '../../api/ShellScript';

function* firstInner(data: any, n?: number): any {
  n = n || 1;
  let count = 0;
  for (let item of ShellScript.iterate(data)) {
    if (count++ < n) {
      yield item;
    } else {
      return;
    }
  }
}

export function first(data: any, n?: number): any {
  const result = firstInner(data, n);
  return ShellScript.formatSameAs(result, data);
}

ShellScript.man(first, 'Gets the first n items.');
