import { ShellScript } from '../../api/ShellScript';

export function reduce(data: any): any {
  let accumulator = arguments.length < 3 ? 0 : arguments[1];
  let callback = arguments.length < 3 ? arguments[1] : arguments[2];
  for (let item of ShellScript.iterate(data)) {
    accumulator = callback(accumulator, item);
  }
  return accumulator;
}

ShellScript.man(
  reduce,
  'Applies a function against an accumulator and each element in the data to reduce it to a single value. An initial value can be provided optionally.'
);
