import { ShellScript } from '../../api/ShellScript';

export function sort(data: any, compareFn?: (a: any, b: any) => number): any {
  const result = [...ShellScript.iterate(data)].sort(compareFn);
  return ShellScript.formatSameAs(result, data);
}

ShellScript.man(sort, 'Sorts the objects (with an optional comparator function, if specified).');
