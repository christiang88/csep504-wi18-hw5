import { ShellScript } from '../../api/ShellScript';

export function last(data: any, n?: number): any {
  n = n || 1;
  let result: any[] = [];
  for (let item of ShellScript.iterate(data)) {
    if (result.length >= n) {
      result.shift();
    }
    result.push(item);
  }
  return ShellScript.formatSameAs(result, data);
}

ShellScript.man(last, 'Gets the last n items.');
