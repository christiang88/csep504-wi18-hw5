import { ShellScript } from '../../api/ShellScript';

export function product(data: any): number {
  let product = 1;
  for (let item of ShellScript.iterate(data)) {
    product *= item;
  }
  return product;
}

ShellScript.man(product, 'Returns the product of the data elements.');
