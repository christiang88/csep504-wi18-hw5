import { ShellScript } from '../../api/ShellScript';
import { clearHistorySymbol } from '../../components/Shell';

export function clear(): any {
  return clearHistorySymbol;
}

ShellScript.man(clear, 'Clears the screen.');
