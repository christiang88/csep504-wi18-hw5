import { ShellScript } from '../../api/ShellScript';

export function each<T>(data: any, callback: (item: T) => void) {
  const iterator = ShellScript.iterate(data);

  for (let item of iterator) {
    callback(item);
  }
}

ShellScript.man(each, 'Executes a provided function once for each item.');
