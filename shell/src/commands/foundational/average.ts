import { ShellScript } from '../../api/ShellScript';

export function average(data: any): number {
  let sum = 0;
  let count = 0;
  for (let item of ShellScript.iterate(data)) {
    sum += item;
    count++;
  }
  return sum / count;
}

ShellScript.man(average, 'Returns the average of the data elements.');
