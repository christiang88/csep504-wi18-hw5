import { ShellScript } from '../../api/ShellScript';

export function print(data: any): any {
  (window as any)['__$ShellScript$pushExpr'](data);
}

ShellScript.man(print, 'Prints values to the console.');
