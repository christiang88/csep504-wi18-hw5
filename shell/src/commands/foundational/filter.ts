import { ShellScript } from '../../api/ShellScript';

function* filterInner<T>(data: any, predicate: (item: T) => boolean) {
  for (let item of ShellScript.iterate(data)) {
    if (predicate(item)) {
      yield item;
    }
  }
}

export function filter<T>(data: any, predicate: (item: T) => boolean): any {
  const result = filterInner(data, predicate);
  return ShellScript.formatSameAs(result, data);
}

ShellScript.man(filter, 'Passes through only values that pass the test implemented by the provided function.');
