import { ShellScript } from '../../api/ShellScript';

function* mapInner<T, T2>(data: IterableIterator<T>, callback: (item: T) => T2) {
  for (let item of data) {
    yield callback(item);
  }
}

export function map<T, T2>(data: T[], callback: (item: T) => T2) {
  return mapInner(ShellScript.iterate(data), callback);
}

ShellScript.man(
  map,
  'Creates new values being the results of calling the provided function on every element in the passed in parameter.'
);
