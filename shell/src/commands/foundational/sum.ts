import { ShellScript } from '../../api/ShellScript';

export function sum(data: any): number {
  let sum = 0;
  for (let item of ShellScript.iterate(data)) {
    sum += item;
  }
  return sum;
}

ShellScript.man(sum, 'Returns the sum of the data elements.');
