import { ShellScript } from '../../api/ShellScript';
import * as React from 'react';

export interface YouTubeResult {
  videoId: string;
}

function youtubeFormatter(data: YouTubeResult) {
  return <iframe width="560" height="315" src={`https://www.youtube.com/embed/${data.videoId}`} />;
}

/** Takes a url to a youtube video and embeds it in the terminal */
export function youtube(url: string): any {
  const youtubeVideoPrefix = 'https://www.youtube.com/watch';
  const videoId = new URLSearchParams(new URL(url).search).get('v');
  if (url.indexOf(youtubeVideoPrefix) === -1 || !videoId) {
    throw new Error('Invalid youtube URL');
  }

  const result = { videoId };
  return ShellScript.format(result, youtubeFormatter);
}

ShellScript.man(youtube, 'Plays the YouTube video at the specified URL.');
