import { ShellScript } from '../../api/ShellScript';
import { sync as touchSync } from 'touch';

export function touch(filename: string) {
  touchSync(filename);
}

ShellScript.man(
  touch,
  'Creates a new, empty file with the specified name or updates the timestamp of the file or directory if it already exists.'
);
