import { ShellScript } from '../../api/ShellScript';
import { unlinkSync } from 'fs';

export function rm(filename: string) {
  unlinkSync(filename);
}

ShellScript.man(rm, 'Deletes the specified file.');
