import { ShellScript } from '../../api/ShellScript';
import * as child_process from 'child_process';

export function exec(path: string) {
  const args = [...arguments].slice(1);
  const result = child_process.spawnSync(path, args);
  if (result.error) {
    throw result.error;
  }
  const output = `${result.stdout}`;
  try {
    return JSON.parse(output);
  } catch {
    return output;
  }
}

ShellScript.man(exec, 'Runs the executable with the specified name and arguments.');
