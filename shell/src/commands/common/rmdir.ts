import { ShellScript } from '../../api/ShellScript';
import { existsSync, rmdirSync } from 'fs';

export function rmdir(name: string) {
  if (existsSync(name)) {
    rmdirSync(name);
  }
}

ShellScript.man(rmdir, 'Removes the specified directory from your filesystem.');
