import { ShellScript } from '../../api/ShellScript';
import { writeFileSync } from 'fs';

function writeJSON(data: any, path: string) {
  writeFileSync(path, JSON.stringify([...data]));
}

function writeLines(data: any, path: string) {
  writeFileSync(path, data.map((item: any) => `${item}`).join('\n'));
}

export function write(data: any, path: string, options?: { format: string }) {
  if (options && options.format === 'json') {
    return writeJSON(data, path);
  } else {
    return writeLines(data, path);
  }
}

ShellScript.man(write, "Writes the data to the specified file. Specify format:'json' to write as JSON.");
