import { ShellScript } from '../../api/ShellScript';
import * as fs from 'fs';
import * as process from 'process';

export function cd(path: string) {
  if (!fs.existsSync(path)) {
    throw new Error(`The path: ${path} does not exist`);
  }

  process.chdir(path);
}

ShellScript.man(cd, 'Changes the current working directory.');
