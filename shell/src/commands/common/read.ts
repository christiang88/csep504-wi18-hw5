import { ShellScript } from '../../api/ShellScript';

const fs = require('fs-sync');
const LineReader = require('n-readlines');

function readJSON(path: string): any {
  return fs.readJSON(path);
}

function* readLines(path: string): IterableIterator<string> {
  const lineReader = new LineReader(path);
  let line;
  while ((line = lineReader.next())) {
    yield line.toString();
  }
}

export function read(path: string, options?: { format: string }): any {
  if (options && options.format === 'json') {
    return readJSON(path);
  } else {
    return readLines(path);
  }
}

ShellScript.man(read, "Reads the specified file from your filesystem. Specify format:'json' to read as JSON.");
