import { ShellScript } from '../../api/ShellScript';
import { existsSync, mkdirSync } from 'fs';

export function mkdir(name: string) {
  if (!existsSync(name)) {
    mkdirSync(name);
  }
}

ShellScript.man(mkdir, 'Creates a new directory with the specified name if it does not already exist.');
