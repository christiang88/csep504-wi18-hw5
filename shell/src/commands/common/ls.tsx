import { ShellScript } from '../../api/ShellScript';
import * as fs from 'fs';
import * as path from 'path';
import * as process from 'process';
import * as dateformat from 'dateformat';
import * as React from 'react';

function* lsInner(dir: string, recurse: boolean): any {
  // Read files and directories at this level
  const files = fs.readdirSync(dir).map(file => {
    const filePath = path.resolve(dir, file);
    const stats = fs.statSync(filePath);
    return {
      type: stats.isDirectory() ? 'dir' : 'file',
      name: path.relative('.', filePath),
      size: stats.isDirectory() ? undefined : stats.size,
      createdDate: dateformat(stats.ctime, 'yyyy-mm-dd HH:MM:ss'),
      lastModifiedTime: dateformat(stats.mtime, 'yyyy-mm-dd HH:MM:ss')
    };
  });

  // Subdirectories
  for (let file of files.filter(file => file.type === 'dir')) {
    yield file;
    if (recurse) {
      const filePath = path.resolve(process.cwd(), file.name);
      for (let file of lsInner(filePath, recurse)) {
        yield file;
      }
    }
  }

  // Files
  for (let file of files.filter(file => file.type !== 'dir')) {
    yield file;
  }
}

export function ls(options?: { recurse: boolean }): any {
  const recurse = options && options.recurse;
  const lsIterator = lsInner('.', !!recurse);
  return ShellScript.formatTable(lsIterator, ['name', 'size', 'createdDate', 'lastModifiedTime']);
}

ShellScript.man(ls, 'Gets a list of files and directories from the current path, or the specified path.');
