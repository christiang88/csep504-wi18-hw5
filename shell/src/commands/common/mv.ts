import { ShellScript } from '../../api/ShellScript';
import { renameSync } from 'fs';

export function mv(source: string, destination: string) {
  renameSync(source, destination);
}

ShellScript.man(mv, 'Moves, or renames, the specfied file or directory on your filesystem.');
