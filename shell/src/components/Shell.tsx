import * as React from 'react';
import * as process from 'process';
import { CommandInput } from './CommandInput';
import { executeCommand } from './executeCommand';
import { getPrintedElement } from './PrintManager';
import { remote } from 'electron';
import { ShellHistory } from './ShellHistory';

export interface ShellState {
  elementsToRender: JSX.Element[];
  path: string;
}

const PathElement = (props: { path: string }) => <span style={{ paddingRight: 3 }}>{props.path}></span>;

const ErrorElement = (props: { errorText: string }) => <span style={{ color: 'red' }}>{props.errorText}</span>;

export const clearHistorySymbol = Symbol('clearHistory');

export class Shell extends React.PureComponent<{}, ShellState> {
  state = {
    elementsToRender: [] as JSX.Element[],
    path: process.cwd()
  };

  private onCommandSubmitted = async (command: string) => {
    let newElementsToRender = [...this.state.elementsToRender];
    newElementsToRender.push(
      <div>
        <PathElement path={this.state.path} />
        {command.split('\n').map(line => <div>{line}</div>)}
      </div>
    );

    try {
      const results: any[] = [];
      (window as any)['__$ShellScript$pushExpr'] = (object: any) => results.push(object);
      await executeCommand(command);

      results.forEach(x => {
        if (x == clearHistorySymbol) {
          newElementsToRender = [];
        } else {
          newElementsToRender.push(getPrintedElement(x));
        }
      });
    } catch (error) {
      newElementsToRender.push(<ErrorElement errorText={error.message} />);
    }

    this.setState({ elementsToRender: newElementsToRender, path: process.cwd() });
  };

  componentDidUpdate() {
    window.requestAnimationFrame(() => (document.body.scrollTop = document.body.scrollHeight));
  }

  render() {
    const { elementsToRender, path } = this.state;

    return (
      <div>
        <ShellHistory lines={elementsToRender} />
        <div>
          <PathElement path={path} />
          <div style={{ display: 'flex' }}>
            <CommandInput onCommandSubmitted={this.onCommandSubmitted} />
          </div>
        </div>
      </div>
    );
  }
}
