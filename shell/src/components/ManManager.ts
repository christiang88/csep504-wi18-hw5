const manMap = new Map<Function, string>();

export function registerMan(func: Function, man: string) {
  manMap.set(func, man);
}

export function getMan(func: Function): string | undefined {
  return manMap.get(func);
}

export function getAllMan(): { name: string; help: string }[] {
  return [...manMap.entries()].map(entry => ({ name: entry[0].name, help: entry[1] }));
}
