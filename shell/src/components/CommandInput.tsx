import * as React from 'react';

export interface CommandInputState {
  inputText: string;
  height: number;
}

class CommandHistoryManager {
  private commandHistory: string[] = [];
  private commandHistoryIndex: number = 0;

  public addCommandToHistory(command: string) {
    this.commandHistory.push(command);
    this.commandHistoryIndex = this.commandHistory.length;
  }

  public getPreviousCommand(): string | undefined {
    if (this.commandHistoryIndex === 0) {
      return undefined;
    }

    this.commandHistoryIndex -= 1;
    return this.commandHistory[this.commandHistoryIndex];
  }

  public getNextCommand(): string {
    if (this.commandHistoryIndex === this.commandHistory.length - 1) {
      return '';
    }

    this.commandHistoryIndex += 1;
    return this.commandHistory[this.commandHistoryIndex];
  }
}

export interface CommandInputProps {
  onCommandSubmitted: (command: string) => void;
}

const lineHeight = 16;

export class CommandInput extends React.PureComponent<CommandInputProps, CommandInputState> {
  private inputRef: HTMLTextAreaElement | null = null;
  private commandHistoryManager = new CommandHistoryManager();

  state = {
    inputText: '',
    height: lineHeight
  };

  private onChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({
      inputText: event.target.value
    });
  };

  private onKeyDown = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (event.keyCode === 13) {
      // If the control key is pressed, just insert a new line in the input box, don't submit the command
      if (event.ctrlKey) {
        this.setState(prevState => ({
          inputText: prevState.inputText + '\n',
          height: prevState.height + lineHeight
        }));
      } else {
        event.preventDefault();

        let input = this.state.inputText;
        if (event.shiftKey && !input.endsWith(')')) {
          input += '()';
        }

        this.commandHistoryManager.addCommandToHistory(input);
        this.props.onCommandSubmitted(input);
        // Todo: stop the flashing by making the text area readonly as the command result instead of reusing the same text area for future commands
        this.setState({
          inputText: '',
          height: lineHeight
        });
      }
    }

    // Up and down arrow should show previous command history, unless we are in multiline mode
    if (this.state.height === lineHeight) {
      let newCommand: string | undefined = undefined;
      if (event.keyCode === 38) {
        newCommand = this.commandHistoryManager.getPreviousCommand();
      } else if (event.keyCode === 40) {
        newCommand = this.commandHistoryManager.getNextCommand();
      }

      if (newCommand !== undefined && newCommand !== this.state.inputText) {
        this.setState({ inputText: newCommand });
      }
    }
  };

  componentDidMount() {
    if (this.inputRef) {
      this.inputRef.focus();
    }
  }

  public render() {
    const { inputText, height } = this.state;

    return (
      <textarea
        ref={input => (this.inputRef = input)}
        style={{ outline: 'none', border: 0, flex: '1', height }}
        onChange={this.onChange}
        value={inputText}
        onKeyDown={this.onKeyDown}
      />
    );
  }
}
