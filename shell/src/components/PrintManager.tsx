import * as React from 'react';
import { SortableTable } from './SortableTable';
import { toIterator } from './toIterator';

const printMap = new WeakMap<object, (data: object) => JSX.Element>();

export function registerPrinter<T>(data: object, printFunction: (data: T) => JSX.Element): object {
  printMap.set(data, printFunction as any);
  return data;
}

export function registerTablePrinter<T>(data: object, columns: string[]): object {
  registerPrinter(data, tablePrinter(columns));
  return data;
}

class SimplePrinter extends React.PureComponent<{ iterator: IterableIterator<any> }, { items: any[] }> {
  state = { items: [] };

  private getNextValueAndUpdateState() {
    const nextValue = this.props.iterator.next();

    if (!nextValue.done) {
      window.setTimeout(() => this.setState({ items: [...this.state.items, nextValue.value] }), 0);
    }
  }

  componentDidMount() {
    this.getNextValueAndUpdateState();
  }

  componentDidUpdate() {
    this.getNextValueAndUpdateState();
    window.requestAnimationFrame(() => (document.body.scrollTop = document.body.scrollHeight));
  }

  render() {
    const { items } = this.state;

    return <pre>{items.join('\n')}</pre>;
  }
}

function tablePrinter(columns: string[]): (data: object) => JSX.Element {
  return (data: any) => <SortableTable data={data} columns={columns} />;
}

function* prependIterator<T>(iterator: IterableIterator<T>, firstItem: T) {
  yield firstItem;

  for (let item of iterator) {
    yield item;
  }
}

function defaultPrinter(object: any) {
  if (typeof object !== 'object' && !Array.isArray(object)) {
    return <pre>{object}</pre>;
  }

  let iterator = toIterator(object);
  const firstItem = iterator.next();

  if (firstItem.done) {
    return <></>;
  }

  iterator = prependIterator(iterator, firstItem.value);

  if (typeof firstItem.value !== 'object') {
    return <SimplePrinter iterator={iterator} />;
  }

  return <SortableTable data={iterator} columns={Object.keys(firstItem.value)} />;
}

export function getPrintedElement(object: object): JSX.Element {
  const printer = printMap.get(object) || defaultPrinter;
  return printer(object);
}

/** Copy the printer associated with source to the destination object. */
export function copyPrinter(destination: object, source: object): object {
  const printer = printMap.get(source);
  if (printer) {
    printMap.set(destination, printer);
  }
  return destination;
}
