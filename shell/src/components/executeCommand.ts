import { ShellScript } from '../api/ShellScript';

import { average } from '../commands/foundational/average';
import { clear } from '../commands/foundational/clear';
import { each } from '../commands/foundational/each';
import { filter } from '../commands/foundational/filter';
import { first } from '../commands/foundational/first';
import { last } from '../commands/foundational/last';
import { man } from '../commands/foundational/man';
import { map } from '../commands/foundational/map';
import { print } from '../commands/foundational/print';
import { product } from '../commands/foundational/product';
import { reduce } from '../commands/foundational/reduce';
import { sort } from '../commands/foundational/sort';
import { sum } from '../commands/foundational/sum';

import { cd } from '../commands/common/cd';
import { exec } from '../commands/common/exec';
import { ls } from '../commands/common/ls';
import { mkdir } from '../commands/common/mkdir';
import { mv } from '../commands/common/mv';
import { read } from '../commands/common/read';
import { rm } from '../commands/common/rm';
import { rmdir } from '../commands/common/rmdir';
import { touch } from '../commands/common/touch';
import { write } from '../commands/common/write';

import { youtube } from '../commands/fun/youtube';

(window as any)['ShellScript'] = ShellScript;

(window as any)['average'] = average;
(window as any)['avg'] = average;
(window as any)['clear'] = clear;
(window as any)['each'] = each;
(window as any)['filter'] = filter;
(window as any)['first'] = first;
(window as any)['head'] = first;
(window as any)['help'] = man;
(window as any)['last'] = last;
(window as any)['man'] = man;
(window as any)['map'] = map;
(window as any)['print'] = print;
(window as any)['prod'] = product;
(window as any)['product'] = product;
(window as any)['reduce'] = reduce;
(window as any)['sort'] = sort;
(window as any)['sum'] = sum;
(window as any)['tail'] = last;
(window as any)['where'] = filter;

(window as any)['$'] = exec;
(window as any)['cat'] = read;
(window as any)['cd'] = cd;
(window as any)['exec'] = exec;
(window as any)['del'] = rm;
(window as any)['dir'] = ls;
(window as any)['ls'] = ls;
(window as any)['mkdir'] = mkdir;
(window as any)['mv'] = mv;
(window as any)['read'] = read;
(window as any)['rm'] = rm;
(window as any)['rmdir'] = rmdir;
(window as any)['touch'] = touch;
(window as any)['write'] = write;

(window as any)['youtube'] = youtube;

export async function executeCommand(command: string): Promise<any> {
  const compiled = await fetch('http://localhost:8081/compile', {
    body: command,
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    }
  }).then(x => x.json());

  if (compiled.error) {
    throw { message: compiled.error };
  }

  const globalEval = eval;
  const result = globalEval(compiled.result);
  return result;
}
