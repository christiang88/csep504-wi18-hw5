function isIterable(object: any) {
  if (!object) {
    return false;
  }
  return typeof object['next'] === 'function';
}

function* arrayToIterator<T extends object>(array: T[]): IterableIterator<T> {
  for (let value of array) {
    yield value;
  }
}

export function toIterator(object: any): IterableIterator<any> {
  if (isIterable(object)) {
    return object;
  }

  let arrayOfObjects = object;
  if (!Array.isArray(object)) {
    arrayOfObjects = [object];
  }

  return arrayToIterator(arrayOfObjects);
}
