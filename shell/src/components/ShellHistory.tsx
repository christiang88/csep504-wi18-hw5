import * as React from 'react';

export interface ShellHistoryProps {
  // Previous lines of shell commands to render
  lines: JSX.Element[];
}

export class ShellHistory extends React.PureComponent<ShellHistoryProps, {}> {
  render() {
    const { lines } = this.props;
    return <>{lines}</>;
  }
}
