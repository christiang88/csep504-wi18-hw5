import * as React from 'react';
import { toIterator } from './toIterator';

export interface SortableTableProps {
  // The data to display
  data: any;

  // The column headings for the table
  columns: string[];
}

export interface SortableTableState {
  items: any[];
  sortedColumn?: string;
  sortDirection?: 'asc' | 'desc';
}

export interface TableHeaderProps {
  title: string;
  sorted?: 'none' | 'asc' | 'desc';
  onClick: (title: string) => void;
}

export class TableHeader extends React.PureComponent<TableHeaderProps, {}> {
  private onClick = () => {
    this.props.onClick(this.props.title);
  };

  render() {
    const { title, sorted } = this.props;

    let chevronCharacter = '';

    if (sorted === 'asc') {
      chevronCharacter = '\u25B4';
    } else if (sorted === 'desc') {
      chevronCharacter = '\u25BE';
    }

    return (
      <th onClick={this.onClick}>
        <span>{title}</span>
        <span style={{ float: 'right' }}>{chevronCharacter}</span>
      </th>
    );
  }
}

function sortFn(a: any, b: any, column: string) {
  if (typeof a[column] === 'number' && typeof b[column] === 'number') {
    return a[column] - b[column];
  } else {
    return a[column] > b[column] ? 1 : -1;
  }
}

function sortTable(items: any[], sortDirection: 'asc' | 'desc', sortedColumn: string): any[] {
  let sortedItems = items.slice().sort((a: any, b: any) => sortFn(a, b, sortedColumn));
  if (sortDirection === 'desc') {
    sortedItems.reverse();
  }
  return sortedItems;
}

export class SortableTable extends React.PureComponent<SortableTableProps, SortableTableState> {
  iterator: IterableIterator<any>;

  constructor(props: SortableTableProps) {
    super(props);
    this.state = { items: [] };
    this.iterator = toIterator(props.data);
  }

  private onColumnClick = (column: string) => {
    let sortDirection: 'asc' | 'desc' = 'asc';
    if (this.state.sortedColumn === column && this.state.sortDirection === 'asc') {
      sortDirection = 'desc';
    }

    this.setState({ sortDirection, sortedColumn: column });
  };

  private getNextValueAndUpdateState() {
    const nextVal = this.iterator.next();

    if (!nextVal.done) {
      window.setTimeout(() => this.setState({ items: [...this.state.items, nextVal.value] }), 0);
    }
  }

  componentDidUpdate() {
    this.getNextValueAndUpdateState();
    window.requestAnimationFrame(() => (document.body.scrollTop = document.body.scrollHeight));
  }

  componentDidMount() {
    this.getNextValueAndUpdateState();
  }

  render() {
    const { columns } = this.props;
    const { items, sortDirection, sortedColumn } = this.state;

    let sortedItems = items;
    if (sortDirection && sortedColumn !== undefined) {
      sortedItems = sortTable(items, sortDirection, sortedColumn);
    }

    return (
      <table>
        <tbody>
          <tr style={{ backgroundColor: 'rgb(68, 114, 196)', color: 'white' }}>
            {columns.map(header => (
              <TableHeader
                key={header}
                title={header}
                onClick={this.onColumnClick}
                sorted={header === sortedColumn ? sortDirection : 'none'}
              />
            ))}
          </tr>
          {sortedItems.map((item, index) => (
            <tr style={{ backgroundColor: index % 2 === 0 ? 'rgb(217,226,243)' : 'white' }} key={item[columns[0]]}>
              {columns.map(column => (
                <td key={column} style={{ padding: '0 7px' }}>
                  {item[column]}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
