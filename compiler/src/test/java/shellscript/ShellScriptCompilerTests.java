package shellscript;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.javascript.jscomp.CompilationLevel;
import com.google.javascript.jscomp.Compiler;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.SourceFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

class ShellScriptCompilerTests {
  private String testResourceDirectory() {
    ClassLoader classLoader = this.getClass().getClassLoader();
    File emptyTestFile = new File(classLoader.getResource("empty").getFile());
    return emptyTestFile.getParent();
  }

  private static boolean isRegressionTest(Path file) {
    return file.getFileName().toString().startsWith("regression_");
  }

  @TestFactory
  Iterator<DynamicTest> dynamicShellScriptTests() throws IOException {
    return Files.walk(Paths.get(testResourceDirectory()))
        .filter(Files::isRegularFile)
        .filter(file -> !isRegressionTest(file))
        .map(
            file ->
                DynamicTest.dynamicTest(
                    "ShellScript test: " + file.toAbsolutePath().getFileName(),
                    () -> {
                      String testFile = new String(Files.readAllBytes(file.toAbsolutePath()));
                      String[] parts = testFile.split("\r?\n-+\r?\n", -1 /* limit */);
                      assertEquals(2, parts.length, "Test file has incorrect format.");
                      String input = parts[0];
                      String expectedOutput = parts[1];
                      String actualOutput = ShellScriptCompiler.compile(input);
                      assertEquals(
                          normalizeJavaScript(expectedOutput), normalizeJavaScript(actualOutput));
                    }))
        .iterator();
  }

  @TestFactory
  Iterator<DynamicTest> dynamicRegressionTests() throws IOException {
    return Files.walk(Paths.get(testResourceDirectory()))
        .filter(Files::isRegularFile)
        .filter(ShellScriptCompilerTests::isRegressionTest)
        .map(
            file ->
                DynamicTest.dynamicTest(
                    "Regression test: " + file.toAbsolutePath().getFileName(),
                    () -> {
                      String input = new String(Files.readAllBytes(file.toAbsolutePath()));
                      String output = ShellScriptCompiler.compile(input);
                      assertEquals(normalizeJavaScript(input), normalizeJavaScript(output));
                    }))
        .iterator();
  }

  private static String normalizeJavaScript(String source) {
    SourceFile externs = SourceFile.fromCode("externs.js", "");
    SourceFile input = SourceFile.fromCode("input.js", source);

    Compiler compiler = new Compiler();
    CompilerOptions options = new CompilerOptions();
    CompilationLevel.WHITESPACE_ONLY.setOptionsForCompilationLevel(options);
    options.setPrettyPrint(true);

    compiler.compile(externs, input, options);
    return compiler.toSource();
  }
}
