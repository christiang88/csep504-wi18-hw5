package shellscript;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import shellscript.grammar.ShellScriptCallSource;
import shellscript.grammar.ShellScriptLexer;
import shellscript.grammar.ShellScriptParser;
import shellscript.grammar.ShellScriptParser.ArgumentsContext;
import shellscript.grammar.ShellScriptParser.ArgumentsExpressionContext;
import shellscript.grammar.ShellScriptParser.EosContext;
import shellscript.grammar.ShellScriptParser.ExpressionNakedLambdaBodyContext;
import shellscript.grammar.ShellScriptParser.ExpressionStatementContext;
import shellscript.grammar.ShellScriptParser.FormalParameterListContext;
import shellscript.grammar.ShellScriptParser.FunctionBodyContext;
import shellscript.grammar.ShellScriptParser.NakedArgumentsContext;
import shellscript.grammar.ShellScriptParser.NakedArgumentsExpressionContext;
import shellscript.grammar.ShellScriptParser.NakedLambdaBodyContext;
import shellscript.grammar.ShellScriptParser.NakedLambdaNoParametersContext;
import shellscript.grammar.ShellScriptParser.NakedLambdaParameterContext;
import shellscript.grammar.ShellScriptParser.NakedLambdaParametersContext;
import shellscript.grammar.ShellScriptParser.NakedLastArgumentsContext;
import shellscript.grammar.ShellScriptParser.NakedNewExpressionContext;
import shellscript.grammar.ShellScriptParser.NewExpressionContext;
import shellscript.grammar.ShellScriptParser.NormalNakedLambdaBodyContext;
import shellscript.grammar.ShellScriptParser.OptionArgumentContext;
import shellscript.grammar.ShellScriptParser.OptionArgumentsContext;
import shellscript.grammar.ShellScriptParser.PipelineContext;
import shellscript.grammar.ShellScriptParser.ProgramContext;
import shellscript.grammar.ShellScriptParser.SingleExpressionContext;
import shellscript.grammar.ShellScriptParserBaseListener;
import shellscript.grammar.ShellScriptRuleContext;

public class ShellScriptCompiler extends ShellScriptParserBaseListener {

  private static ProgramContext parse(String input) {
    CodePointCharStream inputStream = CharStreams.fromString(input);
    ShellScriptLexer lexer = new ShellScriptLexer(inputStream);
    lexer.setUseStrictDefault(true);
    lexer.removeErrorListeners();
    lexer.addErrorListener(new ShellScriptErrorListener());
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    ShellScriptParser parser = new ShellScriptParser(tokens);
    parser.setBuildParseTree(true);
    parser.removeErrorListeners();
    parser.addErrorListener(new ShellScriptErrorListener());
    return parser.program();
  }

  public static String compile(String input) {
    return compile(input, null /* exprPushFunction */);
  }

  public static String compile(String input, String exprPushFunction) {
    ProgramContext tree = parse(input);
    ShellScriptCompiler compiler = new ShellScriptCompiler();
    compiler.setExprPushFunction(exprPushFunction);
    ParseTreeWalker.DEFAULT.walk(compiler, tree);
    return compiler.getSource();
  }

  private static <T> boolean hasAnyParent(ShellScriptRuleContext context, Class<T> clazz) {
    RuleContext current = context;
    while ((current = current.parent) != null) {
      if (current.getClass().equals(clazz)) {
        return true;
      }
    }
    return false;
  }

  private static boolean underFunction(ShellScriptRuleContext context) {
    return hasAnyParent(context, FunctionBodyContext.class)
        || hasAnyParent(context, NakedLambdaBodyContext.class);
  }

  private StringBuilder source = new StringBuilder();

  private String exprPushFunction;

  private String exprResultVariable = "window['" + UUID.randomUUID() + "']";

  private String exprPushUniqueFunction = "window['" + UUID.randomUUID() + "']";

  String getSource() {
    return source.toString();
  }

  boolean hasExprPushFunction() {
    return exprPushFunction != null && !exprPushFunction.isEmpty();
  }

  String getExprPushFunction() {
    return exprPushFunction;
  }

  void setExprPushFunction(String exprPushFunction) {
    this.exprPushFunction = exprPushFunction;
  }

  @Override
  public void exitEveryRule(ParserRuleContext parserRuleContext) {
    ShellScriptRuleContext context = (ShellScriptRuleContext) parserRuleContext;

    if (context.source == null) {
      context.source = new StringBuilder();
      if (context.children != null) {
        for (ParseTree child : context.children) {
          if (child instanceof ShellScriptRuleContext) {
            context.source.append(' ');
            context.source.append(((ShellScriptRuleContext) child).source);
          } else if (child instanceof TerminalNode) {
            TerminalNode terminalNode = (TerminalNode) child;
            int type = terminalNode.getSymbol().getType();
            if (type != -1 /* EOF */) {
              context.source.append(' ');
              context.source.append(child.getText());
            }
          }
        }
      }
    }

    if (hasExprPushFunction()
        && context instanceof ExpressionStatementContext
        && !underFunction(context)) {
      StringBuilder source = new StringBuilder(" ");
      source.append(exprResultVariable);
      source.append(" = ");
      source.append(context.source);
      source.append('\n');
      source.append(exprPushUniqueFunction);
      source.append('(');
      source.append(exprResultVariable);
      source.append(");\n");
      context.source = source;
    }

    if (context instanceof ProgramContext) {
      if (hasExprPushFunction()) {
        StringBuilder source = new StringBuilder(exprPushUniqueFunction);
        source.append(" = ");
        source.append(exprPushFunction);
        source.append(";\n");
        source.append(context.source);
        this.source = source;
      } else {
        this.source = context.source;
      }
    }
  }

  @Override
  public void exitPipeline(PipelineContext context) {
    List<SingleExpressionContext> parts = context.singleExpression();

    // Handle first part
    StringBuilder source = new StringBuilder(parts.get(0).source);

    // Handle subsequent parts
    for (int i = 1; i < parts.size(); i++) {
      SingleExpressionContext part = parts.get(i);
      if (part.callSource == null) {
        throw new ShellScriptException("Piped to non-function call.");
      }

      StringBuilder newSource = new StringBuilder(part.callSource.expression);
      newSource.append("((");
      newSource.append(source);
      newSource.append(')');
      for (StringBuilder argument : part.callSource.arguments) {
        newSource.append(',');
        newSource.append(argument);
      }
      newSource.append(')');
      source = newSource;
    }

    context.source = source;
  }

  @Override
  public void exitArgumentsExpression(ArgumentsExpressionContext context) {
    ShellScriptCallSource callSource = new ShellScriptCallSource();
    callSource.expression = context.singleExpression().source;
    callSource.arguments = context.arguments().argumentsSource;
    context.callSource = callSource;
  }

  @Override
  public void exitNakedArgumentsExpression(NakedArgumentsExpressionContext context) {
    // Set the call source
    ShellScriptCallSource callSource = new ShellScriptCallSource();
    callSource.expression = context.singleExpression().source;
    callSource.arguments = context.nakedArguments().argumentsSource;
    context.callSource = callSource;

    // Naked calls need special compilation logic
    StringBuilder source = new StringBuilder();
    source.append(callSource.expression);
    source.append('(');
    for (int i = 0; i < callSource.arguments.size(); i++) {
      if (i != 0) {
        source.append(',');
      }
      source.append(callSource.arguments.get(i));
    }
    source.append(')');
    context.source = source;
  }

  @Override
  public void exitNewExpression(NewExpressionContext context) {
    ShellScriptCallSource callSource = new ShellScriptCallSource();
    callSource.expression = new StringBuilder(" new ");

    ArgumentsContext arguments = context.arguments();
    if (arguments != null) {
      callSource.expression.append(context.singleExpression().source);
      callSource.arguments = context.arguments().argumentsSource;
    } else if (context.singleExpression() instanceof ArgumentsExpressionContext) {
      // Hack because the original JavaScript grammar nests the arguments incorrectly sometimes
      ArgumentsExpressionContext singleExpression =
          (ArgumentsExpressionContext) context.singleExpression();
      callSource.expression.append(singleExpression.callSource.expression);
      callSource.arguments = singleExpression.callSource.arguments;
    } else if (context.singleExpression() instanceof NakedArgumentsExpressionContext) {
      // Hack because the original JavaScript grammar nests the arguments incorrectly sometimes
      NakedArgumentsExpressionContext singleExpression =
          (NakedArgumentsExpressionContext) context.singleExpression();
      callSource.expression.append(singleExpression.callSource.expression);
      callSource.arguments = singleExpression.callSource.arguments;
    } else {
      callSource.expression.append(context.singleExpression().source);
    }

    context.callSource = callSource;
  }

  @Override
  public void exitNakedNewExpression(NakedNewExpressionContext context) {
    // Set the call source
    ShellScriptCallSource callSource = new ShellScriptCallSource();
    StringBuilder expressionSource = new StringBuilder(" new ");
    expressionSource.append(context.singleExpression().source);
    callSource.expression = expressionSource;
    callSource.arguments = context.nakedArguments().argumentsSource;
    context.callSource = callSource;

    // Naked calls need special compilation logic
    StringBuilder source = new StringBuilder(" new ");
    source.append(callSource.expression);
    source.append('(');
    for (int i = 0; i < callSource.arguments.size(); i++) {
      if (i != 0) {
        source.append(',');
      }
      source.append(callSource.arguments.get(i));
    }
    source.append(')');
    context.source = source;
  }

  @Override
  public void exitArguments(ArgumentsContext context) {
    List<StringBuilder> argumentsSource = new ArrayList<>();
    for (SingleExpressionContext argument : context.singleExpression()) {
      argumentsSource.add(argument.source);
    }
    if (context.lastArgument() != null) {
      argumentsSource.add(context.lastArgument().source);
    }
    context.argumentsSource = argumentsSource;
  }

  @Override
  public void exitNakedArguments(NakedArgumentsContext context) {
    List<StringBuilder> argumentsSource = new ArrayList<>();
    for (SingleExpressionContext argument : context.singleExpression()) {
      argumentsSource.add(argument.source);
    }
    if (context.nakedLastArguments() != null) {
      argumentsSource.addAll(context.nakedLastArguments().argumentsSource);
    }
    context.argumentsSource = argumentsSource;
  }

  @Override
  public void exitNakedLastArguments(NakedLastArgumentsContext context) {
    List<StringBuilder> argumentsSource = new ArrayList<>();
    if (context.lastArgument() != null) {
      argumentsSource.add(context.lastArgument().source);
    }
    if (context.optionArguments() != null) {
      argumentsSource.add(context.optionArguments().source);
    }
    if (context.nakedLambdaArgument() != null) {
      argumentsSource.add(context.nakedLambdaArgument().source);
    }
    context.argumentsSource = argumentsSource;
  }

  @Override
  public void exitOptionArguments(OptionArgumentsContext context) {
    StringBuilder source = new StringBuilder("{");
    List<OptionArgumentContext> options = context.optionArgument();
    for (int i = 0; i < options.size(); i++) {
      if (i != 0) {
        source.append(',');
      }
      source.append(options.get(i).source);
    }
    source.append('}');
    context.source = source;
  }

  @Override
  public void exitOptionArgument(OptionArgumentContext context) {
    StringBuilder source = new StringBuilder(context.propertyName().source);
    source.append(':');
    if (context.singleExpression() != null) {
      source.append(context.singleExpression().source);
    } else {
      source.append(" true ");
    }
    context.source = source;
  }

  @Override
  public void exitNakedLambdaNoParameters(NakedLambdaNoParametersContext context) {
    StringBuilder source = new StringBuilder("($0, $1, $2, $3, $4, $5, $6, $7, $8, $9) => {");
    source.append(context.nakedLambdaBody().source);
    source.append('}');
    context.source = source;
  }

  @Override
  public void exitNakedLambdaParameter(NakedLambdaParameterContext context) {
    StringBuilder source = new StringBuilder("(");
    source.append(context.formalParameterArg().source);
    source.append(") => {");
    source.append(context.nakedLambdaBody().source);
    source.append('}');
    context.source = source;
  }

  @Override
  public void exitNakedLambdaParameters(NakedLambdaParametersContext context) {
    StringBuilder source = new StringBuilder("(");
    FormalParameterListContext formalParameterList = context.formalParameterList();
    if (formalParameterList != null) {
      source.append(formalParameterList.source);
    }
    source.append(") => {");
    source.append(context.nakedLambdaBody().source);
    source.append('}');
    context.source = source;
  }

  @Override
  public void exitExpressionNakedLambdaBody(ExpressionNakedLambdaBodyContext context) {
    StringBuilder source = new StringBuilder(" return ");
    source.append(context.singleExpression().source);
    source.append(';');
    context.source = source;
  }

  @Override
  public void exitNormalNakedLambdaBody(NormalNakedLambdaBodyContext context) {
    context.source = context.functionBody().source;
  }

  @Override
  public void exitEos(EosContext context) {
    context.source = new StringBuilder(";");
  }
}
