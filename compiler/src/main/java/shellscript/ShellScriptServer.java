package shellscript;

import com.google.gson.Gson;
import spark.Spark;
import spark.utils.StringUtils;

public class ShellScriptServer {
  public static void start(int port, String exprPushFunction) {
    Spark.port(port);
    Spark.post(
        "/compile",
        (request, response) -> {
          response.header("Access-Control-Allow-Origin", "*");
          try {
            String result = ShellScriptCompiler.compile(request.body(), exprPushFunction);
            return CompileResponse.fromResult(result);
          } catch (Exception e) {
            String message =
                StringUtils.isEmpty(e.getMessage()) ? "Compilation failed" : e.getMessage();
            return CompileResponse.fromError(message);
          }
        },
        new Gson()::toJson);
  }

  private static class CompileResponse {
    String result;
    String error;

    private static CompileResponse fromResult(String result) {
      CompileResponse compileResponse = new CompileResponse();
      compileResponse.result = result;
      return compileResponse;
    }

    private static CompileResponse fromError(String error) {
      CompileResponse compileResponse = new CompileResponse();
      compileResponse.error = error;
      return compileResponse;
    }
  }
}
