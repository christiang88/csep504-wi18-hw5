package shellscript.grammar;

import org.antlr.v4.runtime.ParserRuleContext;

public class ShellScriptRuleContext extends ParserRuleContext {
  public StringBuilder source;

  public ShellScriptRuleContext() {}

  public ShellScriptRuleContext(ParserRuleContext parent, int invokingStateNumber) {
    super(parent, invokingStateNumber);
  }
}
