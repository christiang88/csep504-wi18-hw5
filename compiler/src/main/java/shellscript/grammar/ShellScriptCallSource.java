package shellscript.grammar;

import java.util.ArrayList;
import java.util.List;

public class ShellScriptCallSource {
  public StringBuilder expression = new StringBuilder();
  public List<StringBuilder> arguments = new ArrayList<>();
}
