package shellscript.grammar;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import shellscript.grammar.ShellScriptParser.ArgumentsContext;
import shellscript.grammar.ShellScriptParser.DoStatementContext;
import shellscript.grammar.ShellScriptParser.ExpressionNakedLambdaBodyContext;
import shellscript.grammar.ShellScriptParser.ExpressionSequenceContext;
import shellscript.grammar.ShellScriptParser.ForInStatementContext;
import shellscript.grammar.ShellScriptParser.ForStatementContext;
import shellscript.grammar.ShellScriptParser.ForVarInStatementContext;
import shellscript.grammar.ShellScriptParser.ForVarStatementContext;
import shellscript.grammar.ShellScriptParser.IfStatementContext;
import shellscript.grammar.ShellScriptParser.ParenthesizedExpressionContext;
import shellscript.grammar.ShellScriptParser.PipelineContext;
import shellscript.grammar.ShellScriptParser.SwitchStatementContext;
import shellscript.grammar.ShellScriptParser.VariableDeclarationContext;
import shellscript.grammar.ShellScriptParser.WhileStatementContext;

/**
 * All parser methods that used in grammar (p, prev, notLineTerminator, etc.) should start with
 * lower case char similar to parser rules.
 */
public abstract class ShellScriptBaseParser extends Parser {
  public ShellScriptBaseParser(TokenStream input) {
    super(input);
  }

  private static <T> boolean under(RuleContext context, Class<T> clazz) {
    return context != null && context.parent != null && context.parent.getClass().equals(clazz);
  }

  private <T> boolean under(Class<T> clazz) {
    return under(_ctx, clazz);
  }

  private <T, U> boolean under(Class<T> class1, Class<U> class2) {
    return _ctx.parent != null && under(_ctx.parent, class1) && under(_ctx, class2);
  }

  protected boolean maybeNaked() {
    return (under(PipelineContext.class)
        || under(VariableDeclarationContext.class)
        || under(ArgumentsContext.class)
        || under(ExpressionNakedLambdaBodyContext.class)
        || under(IfStatementContext.class, ExpressionSequenceContext.class)
        || under(DoStatementContext.class, ExpressionSequenceContext.class)
        || under(WhileStatementContext.class, ExpressionSequenceContext.class)
        || under(ForStatementContext.class, ExpressionSequenceContext.class)
        || under(ForVarStatementContext.class, ExpressionSequenceContext.class)
        || under(ForInStatementContext.class, ExpressionSequenceContext.class)
        || under(ForVarInStatementContext.class, ExpressionSequenceContext.class)
        || under(SwitchStatementContext.class, ExpressionSequenceContext.class)
        || under(ParenthesizedExpressionContext.class, ExpressionSequenceContext.class));
  }

  protected boolean maybeArguments() {
    return !here(ShellScriptParser.WhiteSpaces);
  }

  /** Short form for prev(String str) */
  protected boolean p(String str) {
    return prev(str);
  }

  /** Whether the previous token value equals to @param str */
  protected boolean prev(String str) {
    return _input.LT(-1).getText().equals(str);
  }

  protected boolean notLineTerminator() {
    return !lineTerminatorAhead();
  }

  protected boolean notOpenBraceAndNotFunction() {
    int nextTokenType = _input.LT(1).getType();
    return nextTokenType != ShellScriptParser.OpenBrace
        && nextTokenType != ShellScriptParser.Function;
  }

  protected boolean closeBrace() {
    return _input.LT(1).getType() == ShellScriptParser.CloseBrace;
  }

  /**
   * Returns {@code true} iff on the current index of the parser's token stream a token of the given
   * {@code type} exists on the {@code HIDDEN} channel.
   *
   * @param type the type of the token on the {@code HIDDEN} channel to check.
   * @return {@code true} iff on the current index of the parser's token stream a token of the given
   *     {@code type} exists on the {@code HIDDEN} channel.
   */
  private boolean here(final int type) {

    // Get the token ahead of the current index.
    int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
    Token ahead = _input.get(possibleIndexEosToken);

    // Check if the token resides on the HIDDEN channel and if it's of the
    // provided type.
    return (ahead.getChannel() == Lexer.HIDDEN) && (ahead.getType() == type);
  }

  /**
   * Returns {@code true} iff on the current index of the parser's token stream a token exists on
   * the {@code HIDDEN} channel which either is a line terminator, or is a multi line comment that
   * contains a line terminator.
   *
   * @return {@code true} iff on the current index of the parser's token stream a token exists on
   *     the {@code HIDDEN} channel which either is a line terminator, or is a multi line comment
   *     that contains a line terminator.
   */
  protected boolean lineTerminatorAhead() {

    // Get the token ahead of the current index.
    int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
    Token ahead = _input.get(possibleIndexEosToken);

    if (ahead.getChannel() != Lexer.HIDDEN) {
      // We're only interested in tokens on the HIDDEN channel.
      return false;
    }

    if (ahead.getType() == ShellScriptParser.LineTerminator) {
      // There is definitely a line terminator ahead.
      return true;
    }

    if (ahead.getType() == ShellScriptParser.WhiteSpaces) {
      // Get the token ahead of the current whitespaces.
      possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 2;
      ahead = _input.get(possibleIndexEosToken);
    }

    // Get the token's text and type.
    String text = ahead.getText();
    int type = ahead.getType();

    // Check if the token is, or contains a line terminator.
    return (type == ShellScriptParser.MultiLineComment
            && (text.contains("\r") || text.contains("\n")))
        || (type == ShellScriptParser.LineTerminator);
  }
}
