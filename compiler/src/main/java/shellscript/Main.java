package shellscript;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
  private static void startServer(int port, String exprPushFunction) {
    ShellScriptServer.start(port, exprPushFunction);
  }

  private static String readStdIn() {
    StringBuilder input = new StringBuilder();
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNextLine()) {
      input.append(scanner.nextLine());
      input.append('\n');
    }
    return input.toString();
  }

  private static String readFile(String path) throws IOException {
    return new String(Files.readAllBytes(Paths.get(path)));
  }

  private static void writeFile(String path, String contents) throws IOException {
    Files.write(Paths.get(path), contents.getBytes());
  }

  public static void main(String[] args) throws Exception {
    // Server mode
    if (args.length > 0 && args[0].equals("-s")) {
      int port = args.length > 1 ? Integer.parseInt(args[1]) : 4567;
      String exprPushFunction = args.length > 2 ? args[2] : null;
      startServer(port, exprPushFunction);
      return;
    }

    // File mode
    String input = args.length < 1 ? readStdIn() : readFile(args[0]);
    String output = ShellScriptCompiler.compile(input);
    if (args.length < 2) {
      System.out.print(output);
    } else {
      writeFile(args[1], output);
    }
  }
}
