package shellscript;

public class ShellScriptException extends RuntimeException {
  public ShellScriptException() {
    super();
  }

  public ShellScriptException(String message) {
    super(message);
  }

  public ShellScriptException(String message, Throwable cause) {
    super(message, cause);
  }

  public ShellScriptException(Throwable cause) {
    super(cause);
  }
}
